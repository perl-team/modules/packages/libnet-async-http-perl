Source: libnet-async-http-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libcompress-bzip2-perl <!nocheck>,
                     libfuture-perl <!nocheck>,
                     libhttp-cookies-perl <!nocheck>,
                     libhttp-message-perl <!nocheck>,
                     libio-async-perl <!nocheck>,
                     libio-async-ssl-perl <!nocheck>,
                     libmetrics-any-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libstruct-dumb-perl <!nocheck>,
                     libtest-metrics-any-perl <!nocheck>,
                     libtest2-suite-perl <!nocheck>,
                     liburi-perl <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-async-http-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-async-http-perl.git
Homepage: https://metacpan.org/release/Net-Async-HTTP
Rules-Requires-Root: no

Package: libnet-async-http-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libfuture-perl,
         libhttp-message-perl,
         libio-async-perl,
         libmetrics-any-perl,
         libscalar-list-utils-perl,
         libstruct-dumb-perl,
         liburi-perl
Recommends: libcompress-bzip2-perl,
            libio-async-ssl-perl
Description: module to use HTTP with IO::Async
 Net::Async::HTTP implements an asynchronous HTTP user agent. It sends
 requests to servers, returning Future instances to yield responses when they
 are received. The object supports multiple concurrent connections to servers,
 and allows multiple requests in the pipeline to any one connection. Normally,
 only one such object will be needed per program to support any number of
 requests.
 .
 As well as using futures the module also supports a callback-based interface.
 .
 Net::Async::HTTP optionally supports SSL connections, if IO::Async::SSL is
 installed. If so, SSL can be requested either by passing a URI with the https
 scheme, or by passing a true value as the SSL parameter.
